#include"storage.h"
void finddownmaxlayer(int* aptr)//寻找下行请求的最高楼层 
{
     int isfirstfind=1; 
     LISTNODEPTR currentptr_down;
     currentptr_down=headptr;
     while(currentptr_down!=NULL){
           if(currentptr_down->direction==2||currentptr_down->direction==0){
               firstfind=0;//表示确定到了目标楼层 
               if(isfirstfind==1){
                            *aptr=currentptr_down->floor;
                            isfirstfind=0;//第一次找到下行请求楼层，设为目标楼层 
               }
               else if(currentptr_down->floor>*aptr){
                      *aptr=currentptr_down->floor;//找到更高的下行请求楼层，刷新目标楼层 
               }
           }
           currentptr_down=currentptr_down->nextptr;
     }
}
