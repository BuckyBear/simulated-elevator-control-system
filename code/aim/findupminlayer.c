#include"storage.h"
void findupminlayer(int* aptr)//寻找上行请求的最低楼层 
{
     int isfirstfind=1;
     LISTNODEPTR currentptr_up;
     currentptr_up=headptr;
     while(currentptr_up!=NULL){
           if(currentptr_up->direction==0||currentptr_up->direction==1){
               firstfind=0;//表示确定到目标楼层 
               if(isfirstfind==1){
                            *aptr=currentptr_up->floor;
                            isfirstfind=0;//第一次找到上行请求楼层，设为目标楼层； 
               }
               else if(currentptr_up->floor<*aptr){
                      *aptr=currentptr_up->floor;//找到更低的上行请求楼层，更新目标楼层 
               }
           }
           currentptr_up=currentptr_up->nextptr;
     }
}     
