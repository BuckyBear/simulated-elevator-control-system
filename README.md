# Simulated Elevator Control System



## Overview

Simulated elevator control system is created by Hui Cai as __a course project of Programming Practice(2013Spring)__ of Beijing University of Posts and Communication. 

The project uses C and *EasyGraphic* *egG* (an open source C language graphic animation function library) to simulate a single-car lift’s functions. It includes input, run control, and output modules.

1. __Input module__: supports input via click, command line, read file, and animated interface click. 
2. __Run control module__: controls the elevator’s uplink, downlink, and docking status; modify or cancel the target floor. In addition, it supports both first-come first-served and shortest-seek-time-first call strategies. 
3. __Output module__: displays the lighting status for each floor and inside the elevator, the elevator’s direction indicator, the elevator’s position and operation status, and the log file output.

## Requirement

The executable file of simulated elevator control system and its dependent animated static link library are only used on the Windows platform.



## Usage

1. Download the exe file folder and run the lift_system.exe on Win platform.

2. Chose the call strategy of the lift  by clicking the button above the lift.

   * A:  first come first served (default)
   * B:  shortest seek time first 

   Note: Some of the prompts on the display are in Chinese.


3. Click  uplink or downlink buttons on different floors and the open, close or the target floors button inside the elevator to simulate the running process of a lift.

4. Project Structure

   <img src="/images/project structure.PNG" style="width: 400px; align:left;" />

## Design

1. __Project objectives__

   Achieve single elevator function

   * The elevator has idle, up, down, and docked operating states.
   * There are target floor buttons and switch door buttons in the elevator.
   * There are up and down call buttons on each floor.
   * The minimum safety interval between the start and stop of the elevator is one floor. Therefore, when the elevator leaves the X layer, the X+1 layer presses the call button and cannot be docked. Similarly, when the elevator descends from the Y layer, the Y-1 layer presses the call button and cannot be docked.
   * The elevator should run at a constant speed of 5 seconds per floor.
   * When the elevator is farther than the minimum safe interval from the target floor, the passenger in the elevator is allowed to click the target button again to cancel the target floor.

2. __Elevator configuration__

   * There are 1 elevator in total.
   * There are _maxfloor_ floors and the default _maxfloor_=9.
   * There are two buttons on each layer in the middle layer. The bottom layer has only the up button, and the bottom layer has only the down button. Each layer has a corresponding indicator. When the light is on, the button has been pressed. If the upstream or downstream request of this layer has been responded, the indicator is off.
   * There are a total of _maxfloor_ target buttons in the elevator, indicating that there are passengers on the floor below the elevator. There is an indicator light to indicate if the button has been pressed. When the passenger presses the button, the button indicator light is on, and if the elevator has been docked at the floor, the button indicator is off.
   * There is also a start button (_GO_). When the elevator stops at a certain floor, it will continue to operate after receiving the _GO_ message. If you do not get the _GO_ information, wait for a while and continue to run automatically.
   * A direction indicator light is arranged in the elevator to indicate the current elevator running direction.
   * When the elevator is greater than the shortest safety interval from the target floor, the passenger in the elevator is allowed to click the target button again to cancel the target floor.

3. __Operation control of the elevator__

   * The initial state of the elevator is that the elevator is on the first floor and all buttons are not pressed.
   * Passengers can press any of the target buttons and call buttons at any time. The floor corresponding to the call and the target may not be the floor where the elevator is currently running.
   * If the elevator is coming to the I floor and is located between the I floor and the adjacent layer (I-1 layer when running up or I+1 layer when running down), it will not respond to this because of security considerations. Level I target or request. If the elevator passes through the I floor and runs between the I floor and the next floor, in order to directly respond to the I-level target or request that occurs at this time, it must reach at least the next floor in the running direction and then turn to the I floor ( Assume that there is no extra time for the U-turn. If the I floor is not just the floor that has just passed, you can make a U-turn at any position. At this time, the first floor that passes after the U-turn cannot be stopped.
   * The elevator system analyzes and responds to randomly occurring calls and targets in accordance with some predefined strategy.
   * External factors such as the number of passengers (which may cause changes in the length of the call) are not considered. Assume that the elevator runs on the first floor for 5 seconds, and the time to stop the target floor, the upper and lower passengers and the elevator continue to run is 5S.
   * When the elevator stops at a certain floor, the passengers on this floor will not respond if they mistakenly press the target or the call button.
   * After the elevator stops at a certain layer, if there is no target and call, the elevator is in a non-directional state, and the direction indicator is completely off. Otherwise, the indicator in a certain direction in the elevator is on, indicating that the elevator will run in that direction. The elevator will continue to run immediately after receiving the "_GO_ " signal. If there is no GO signal, the elevator will continue to operate after waiting for the passengers to get on and off and the elevator to continue running.
   * When a target (call) has been serviced, the corresponding indicator should be extinguished.

## 3rd Party Libraries

EasyGraphic egG (Egg) is an open source software developed by Sun Zhigang, a professor at the School of Computer Science and Technology at Harbin Institute of Technology. It uses the GPL protocol. Its design goal is to develop an extremely simple and intuitive graphic animation function library under the Win32 platform. C language beginners can use it to develop graphics programs such as animations and games.

## Other words

Thanks for reading.

The project is simple but significant to me. When I was a freshman, it was the first time that I developed a system by using C. I learned how to link different code files and let them work together, and get to know some basic software engineering ideas. In addition, it was my first time to think hard to apply algorithms to practical projects and solve problems in life. Motivated by the success of the course project, I explored in computer science more deeply and realized many more complicated projects later, however, this imperfect project  with many drawbacks still means a lot to me. Thanks to my instructor of Programming Practice, who leads me to the code world.





